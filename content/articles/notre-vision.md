+++
title = "Vision"
slug = "keep-cabo-verde-green-vision"
language = "fr"
themes = ["travel"]
date_published = "2020-12-01"
image = "/media/uploads/caboverdeimg_2858.jpg"
image_caption = "Baia das Gatas, ilha de Sao Vicente"
+++
Bienvenue à tous sur le blog de KEEPCABOVERDEGREEN

Nous avons l'intime conviction que, comme son nom l'indique, le Cap-Vert ou Cabo Verde est une destination verte et écologique. C'est pourquoi nous voulons mettre en valeur cette destination et ses habitants.

Soyons inventif et créatif afin de trouver des solutions attrayantes pour tous, éco-touristes visitant ce pays, solutions préservant notre écosystème et profitant à la population locale. 

Vous avez cet état d'esprit GREEN et voulez rester informer de nos activités, ou bien participer, proposer des projets? Vous êtes amoureux du CAP VERT? Rejoignez nous! Contact: adelinoduarte27@gmail.com