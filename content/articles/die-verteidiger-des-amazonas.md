+++
title = "Die Verteidiger des Amazonas"
serie = "que-des-numeros-6-dans-ma-team"
slug = "die-Verteidiger-des-Amazonas"
draft = true
language = "de"
themes = ["Inspirations"]
date_published = "2021-03-23"

[[links]]
link = "Ein Beispiel der Nekro-Politik: [Jair Bolsonaro stoppt Corona-Maßnahmen in Indigenengebieten ](https://www.zeit.de/politik/ausland/2020-07/brasilien-indigene-voelker-coronavirus-massnahmen-jair-bolsonanro?fbclid=IwAR32zA_EunMZmF2aR32g2u8kR4kTKGLjWy-2FbauY7wBUcYHO3TIb1JALGs&utm_referrer=https%3A%2F%2Fpublicservices.gitlab.io%2Fkeepcaboverdegreen%2Fadmin%2F)"

[[links]]
link = "Zeit.de Artikel: [Bergbaukonzern Vale zahlt nach Staudammbruch Milliardenentschädigung](https://www.zeit.de/gesellschaft/2021-02/brasilien-dammbruch-brumadinho-vale-entschaedigung-einigung?wt_zmc=sm.int.zonaudev.twitter.ref.zeitde.redpost.link.x&utm_medium=sm&utm_source=twitter_zonaudev_int&utm_campaign=ref&utm_content=zeitde_redpost_link_x&utm_referrer=https%3A%2F%2Fpublicservices.gitlab.io%2Fkeepcaboverdegreen%2Fadmin%2F)"
+++
Nachdem ich den Artikel über Ailton Krenak geschrieben habe, will ich andere Verteidiger des Amazonas vorstellen, durch kleine Porträte.

Chef Raoni von den Kayapo Völker

Der Chef und Kämpfer Raoni Metuktire (geboren im 1932) hat die ganze Welt bereist um die Öffentlichkeit für den Kampf der indigenen Völker zu sensibilisieren. Seit mehreren Jahrzehnten (1989!) versucht er aufmerksam auf das Thema Amazonas zu machen und internationale Hilfe zu bekommen. 

Chef Raoni verweigert die Entwaldung, die Errichtung Staudämme mit verheerenden ökologischen Folgen und kämpft um die Demarkation und den Schutz der angestammten Länder wobei die autochthone Völker und das Leben im Wald endlich im Frieden leben können. 

Einer seiner großen Sieg ist die Vereinigung verschiedene Reservate zu dem existierenden Xingu Park.

Die Villas-Bôas Brüder

Der Indigene Xingu Park ist eigentlich die perfekte Übergang um die drei Brüder Orlando, Claudio und Leonardo vorzustellen, die in der 1950. Jahren den ersten friedlichen Kontakt mit Raoni und seinem Stammen aufgebaut haben.

1943 nahmen die junge Brüder an einem staatlichen Programm genannt "der Marsch nach Westen" teil und unternahmen eine Expedition im Amazonas, ein Wald in dieser Zeit sehr unbekannt.

Sie bauen daher den ersten Kontakt mit vielen isolierten Völker im Wald auf. Ihre Aufgeschlossenheit und Humanismus ermöglichen ein Vertrauensklima. Da sie mit die indigenen Völker zusammen leben, begann ein gegenseitig Austausch und dadurch entsteht eine Freundschaft.

Die Brüder Villas-Bôas sind sich der Tatsache bewusst, dass die Expedition Zweck rein käuflich und eine solche Mission der "brasilianischen Zivilisation in Namen des Fortschritts" im Amazonas wirkt zerstörerisch. Sie sind gleichzeitig das Gift und das Antidot.

Das Gift denn sie nehmen auch wenn mittelbar am Prozess der Enteignung der indigenen Länder von der Weißen. Zudem übertragen sie Krankheiten, die sterblich für die nicht immun autochthone Völker sind.

Das Antidot denn sie erheblich helfen durch die medizinische Versorgung und durch die Erstellung des Plan für den Aufbau eines Gebiet für die indigene Völker. Im Jahr 1961 schaffen die Villas-Bôas Brüder den 26 000 km2 groß Indigene Park Xingu, in dem 16 ethnische Gruppen wohnen, zu kreieren.

Joenia Wapixana

Joenia Wapixana (geboren im 20. April 1974) ist die erste weibliche indigene Abgeordnete im brasilianischen Kongress (sie wurde 2018 gewählt) und war auch die erste indigene Anwältin (1997). Sie spielt eine wichtige Rolle im Kampf um die Demarkation und erhielt im Jahr 2009 einen bedeutenden gerichtlichen Sieg vor dem oberster Gerichtshof. Außerdem ist sie sehr aktiv auf die politische Bühne gegen die aktuelle Regierung "necropolitics".

Über das Thema "necropolitics". In unserer modernen Gesellschaften, besitzt ein Staat die Macht um " jemandem sterben zu lassen", um " jemandem leben zu lassen" oder um "jemanden vor dem Tod aussetzen". Regiert er oder verweigert er Gesetze zu erlassen abhängig von der Antwort auf die Fragestellung "welches Leben zählt".

Achille Mbembé (Philosoph, Historiker, Politolog und Universitätslehrer, 1957 geboren) erklärt uns von dieser "ausgeübt vom Souverän Macht zu töten" mit dem Konzept "necropolitics".

Eine Politik der Ausschließung eines fiktionales Feindes je nach Ethnizität, Geschlecht oder sexuelle Orientierung wird offensichtlich in Brasilien umgesetzt. Die Praxis der Macht sind die der Rechtsextrem: Rassismus,

Chico Mendes, der Seringueiro

Chico Mendes wird so oft von Ailton Krenak zitiert, dass ich sollte ihn recherchieren.

Im Kampf um den Schutz des Amazons, hatten die indigene Völker einen Alliierter gefunden: Die Seringueiros. Sie sind portugiesischen Besiedlern, die damals Feinde der indigenen Völker waren. Sie hatten beschlossen in dem Wald zu leben sodass sie die lukrative Aktivität der Kautschukgewinnung und die Sammlung der brasilianischen Nüsse ausüben konnten. Im Verlauf der Zeit, haben die Seringueiros den Wald gut kennengelernt und nutzen ihn nachhaltig.

1985 ist Chico Mendes der Initiator einer Vereinigung der Völker des Walds als er die Seringueiros, Ribeirinhos (die Flüsse entlang lebende Gemeinschaften), Quilombolas (Gemeinschaften meist aus afrikanischen und indigenen Herkunft, deren Vorfahren (Sklaven) aus den Plantagen flüchteten) und indigene Völker zusammenbrachte.

Er ist eine zentrale Figur in diesem sozialen und umweltlichen Kampf, der im Amazon gegen den Staat und die reiche Grundbesitzer stattfindet. Mit seinem Kameraden von der Gewerkschaft, bevorzugt er die Nutzung friedlichen Methoden gegen bewaffnete Grundbesitzer und Wachleute.

1988 gewann er beim Gerichtshof dank einem ökonomischen Argument: die Schaffung eines "extraktiviste" Gebiet, in dem die traditionelle Einwohner das Recht haben, den Wald nachhaltig zu nutzen und ihre Tradition und Brauchtum zu bewahren.

1988 wurde Chico von Schergen eines Grundbesitzer ermordet. Chico Mendes ist noch heute ein Symbol des Kampf um den Schutz des Amazons. Im Film "The Burning Season" (1994) verkörpert der Schauspieler Raul Julia die Rolle von Chico.

Sonia Guajajara

Sona Guajajara (geboren im 6. März 1974) ist eine Umweltschützerin und Menschenrechtsverteidigerin, die im Jahr 2015 die Auszeichnung des kulturellen Verdienst erhielt. Sie ist eine Anführerin der APIB (Vereinigung der indigenen Völker Brasiliens) die mehr als 300 ethnische Gruppen vertrete.

Sie befürwortet ein Ökosozialismus und ihre Stimme auf die brasilianische politische Bühne zunimmt. Auf die internationale Ebene anprangert APIB beispielsweise die Banken BNP und Societe Général, die etliche Milliarden Euros in großen Exportunternehmen der Viehwirtschaft und Soja-Anbau anlegen und mit ihnen zusammenarbeiten. Diese Unternehmen sind aber mitschuldig der Abholzung und wohl informiert über der Territorialstreit und resultierende Spannungen.

Der Anführer Marçal de Souza oder Tupa-i hatte eine Rede adressiert am Papst Jean Paul II im Juni 1980 gehalten:

> *"Nossas terras são invadidas, nossas terras são tomadas, os nossos territórios são invadidos… Dizem que o Brasil foi descoberto. O Brasil não foi descoberto não, o Brasil foi invadido e tomado dos indígenas do Brasil. Essa é a verdadeira história".*

*"Unsere Länder sind invadiert, unsere Länder sind genommen, unsere Gebiete sind invadiert...Sie sagen, dass Brasilien entdeckt wurde. Brasilien wurde nicht entdeckt, Brasilien wurde invadiert und entnommen von den Indigenen aus Brasilien. Diese ist die wahre Geschichte."*

> "Da ist mein ganzes Leben, da befindet sich meine Seele. Wenn ihr mir dieses Land vorenthaltet, nimmt ihr mein Leben weg." Marcos Veron

Dieser Guarani Anführer wurde am 13. Januar 2003 ermordet, im Alter 75, während einem Versuch nach sein angestammtes Land zurückzukehren, das verletzt von Eingriffe und illegale Siedlungen der große Ranch-Eigentümer im Bundesstaat Mato Grosso an der Grenze mit Paraguay.

Der yanomami Anführer Davi Kopenawa (geboren im Jahr 1956), Preisträger des 2019 alternativen Nobelpreis, fordert die internationale Gemeinschaft darauf, dass sie die ungeschützte Schützer des gefährdeten Walds Hilfe leisten. Wenn Amazonas, eines der Lungen unserer Erde, in Gefahr ist, dann sind wir eben uns die Humanität in Gefahr.

In der Rest des Amazonas: Maxima Acuna aus Peru

Die Umweltschützerin und Landwirtin aus Peru, Maxima Acuna, erhielt 2016 eine Auszeichnung vergleichbar mit dem "Umweltschutz Nobel Preisen" Goldman Preis für die Umwelt, die ihre Mut gegen eine gigantische Amerikaner Konzern.

Im Jahr 2011 wurde sie und ihre Familie aus ihrem Grundstück von einer Miliz beauftragt bei einem Goldminenunternehmen mit Hilfe von der Polizei brutal vertrieben. Das Unternehmen hatte sich den nebenliegende See angeeignet.

Nun findet ein langer und erschöpfender Rechtsstreit vor der amerikanischen Gerichtshöfe. Zunächst schuldig verurteilt, sie gewann den letzte Prozess, der die Auflösung des Minenprojekt fordert. Peru geblendet durch eine ultraliberale Sicht, gibt der Multinationalen Unternehmen seinen Amazonas-Wald auf und vernachlässigt die Rechte der indigenen Völker.

In Bolivien sind die Indigene Nationen verfassungsrechtlich anerkannt da ist der Bolivianische Staat "plurinational" erklärt. Die berühmte wiphala Fahne vertretet die Indigene Nationen SüdAmerikas.

Rassist, Klimaskeptiker und autoritär, Bolsonaro anhand seiner verhassten Rede gegenüber den indigenen Völker und den Umwelt-Nichtregierungsorganisationen, ermutigt Bauer, Förster und illegale Goldsuchern (garimpeiros) die angestammte Länder zu rauben, sie in Brand zu setzen oder zur Sache kommen Umweltaktivisten zu ermorden.

Er ist bereit die verfassungsrechtliche Hindernisse zu beseitigen sodass er die Demarkation aufhebt oder sperrt. Durch die Anwendung der Strategie des "Laisser-faire", freut er sich heimlich über die kriminelle Brandanschläge und die Zunahme der Entwaldung. Denn seine ultraliberale Agenda ist ganz klar: Die Reichtümer im Amazonas plündern.

In Chile ein Referendum ermöglicht die Erarbeitung der Verfassung. Damit haben die Mapuche Völker die Hoffnung letztendlich verfassungsgemäß anerkannt zu werden. In Bolivien sind bereits die indigene Nationen verfassungsgemäß anerkannt deshalb gilt der Bolivianische Staat als Plurinationaler und die wiphala Flagge stellt die verschiedene indigene Stämme des Lateinamerikas vor.

In Guyana: Frankreich hat die Konvention 169 vom Internationalen Arbeitsorganisation Übereinkommen nicht unterschrieben. Dieses Übereinkommen gilt doch als ein effizientes Mittel für den Schutz des Rechtes der indigenen Völker. Die unzählbaren toxischen Goldminenprojekte (Nutzung des umweltschädliches Zyanid, Abholzung, toxische Abfälle Einlagerung anhand Dämme) zeigen das geringe Interesse Frankreichs den Wald zu schonen und die indigene Völker zu schützen.

Lokal Handeln für globale Wirkung

Das Kampf der indigenen Völker um Anerkennung, Würde und Land sowohl in Brasilien als auch in ganz Südamerika scheint mehr und mehr Sichtbarkeit zu gewinnen während gleichzeitig verschärfen sich die Spannungen und Drücke denn der hohe Goldkurs die Goldgräber wahnsinnig macht.

Ich stehe auf dem Standpunkt, dass die Waldschützer die echte Umweltschützer sind. Durch leicht lokale Handeln, in dem sie den Wald schonen, ausüben sie positive globale Wirkung.

Wir Einwohnern des Nord sollten ebenfalls Bewusst sein, dass jede unsere lokale Handeln eine globale Wirkung haben. Die Aktualität zeigt uns, dass das Schweigen den systemischen Angreifer begünstigt und deshalb hilft keinerlei bei der erforderlichen Veränderung.

Durch unsere Handeln, Zuhören und Stimmen könnten wir Alliierte der Indigenen Völker sein?

Adelino Duarte, 2021