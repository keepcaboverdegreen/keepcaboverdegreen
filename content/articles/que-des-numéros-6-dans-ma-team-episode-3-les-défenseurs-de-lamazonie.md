+++
title = "Défenseurs de l'Amazonie"
serie = "que-des-numeros-6-dans-ma-team"
slug = "defenseurs-de-amazonie"
language = "fr"
themes = ["Inspirations"]
date_published = "2021-03-10"
image = "/media/uploads/e625cb32-sonia-guajajara-credito_-mídia-ninja-1-.jpg"
image_caption = "\n\n\n\n\n\n\n\n\n\nSônia Guajajara (source: Midia NINJA)"

[[links]]
link = "[Nécropolitique](https://www.cairn.info/article.php?ID_ARTICLE=RAI_021_0029) de Achille Mbembe, article [](https://www.cairn.info/article.php?ID_ARTICLE=RAI_021_0029)dans cairn.info"

[[links]]
link = "[Necropolitics](https://criticallegalthinking.com/2020/03/02/achille-mbembe-necropolitics/), de Achille Mbembé (en anglais) article [](https://criticallegalthinking.com/2020/03/02/achille-mbembe-necropolitics/)"

[[links]]
link = "Excellente vidéo YouTube [Voice of the Amazon](https://youtu.be/Ii0ypePaZ1o) (50 min) sur Chico Mendes"

[[links]]
link = "[Les orpailleurs illégaux de l'Amazonie en Guyane](https://youtu.be/5VdnaLzPFjI) (les garimpeiros), vidéo YouTube de Arte avec François-Michel Le Tourneau"

[[links]]
link = "[Qui sont les pyromanes de l'Amazonie](https://www.bastamag.net/Amazonie-incendies-deforestation-Bolsonaro-soja-boeufs-fazendeiros-corruption-peuples-autochtones)? Article de Bastamag.net [](https://www.bastamag.net/Amazonie-incendies-deforestation-Bolsonaro-soja-boeufs-fazendeiros-corruption-peuples-autochtones)"

[[links]]
link = "Article sur[ la Guyane et la convention 169](https://la1ere.francetvinfo.fr/guyane-quoi-consiste-convention-169-oit-reclamee-peuples-autochtones-459269.html) de l'OIT non signée par la France. La convention 169 en intégralité [ici](http://www.ilo.org/dyn/normlex/fr/f?p=NORMLEXPUB:12100:0::NO::P12100_ILO_CODE:C169)"

[[links]]
link = "[Le combat du peuple Munduruku au Brésil](https://youtu.be/ANMNpJsbqnY), vidéo YouTube"

[[links]]
link = "\"[Chief Marcos Veron](https://www.theguardian.com/news/2003/jan/28/guardianobituaries)\", article The Guardian"

[[links]]
link = "[43 femmes indigènes du Brésil et Amérique du sud](https://catarinas.info/43-mulheres-indigenas-do-brasil-e-da-america-latina-para-se-inspirar/), article en portugais"

[[links]]
link = "Témoignage de Maxima Acuna, \"[I will never give up my land](https://newint.org/features/2016/07/01/interview-maxima-acuna/)\""

[[links]]
link = "Article \"[BNP Paribas, la cible de chefs indigènes du Brésil pour son rôle dans la destruction de l’Amazonie](http://planeteamazone.org/actualites/bnp-paribas-cible-de-chefs-indigenes-du-bresil-pour-son-role-dans-la-destruction-de-lamazonie/)\" [](http://planeteamazone.org/actualites/bnp-paribas-cible-de-chefs-indigenes-du-bresil-pour-son-role-dans-la-destruction-de-lamazonie/)par Planetamazone.org"

[[links]]
link = "article: Parc autochtone du [Xingu](https://fr.buru-news.com/parque-ind-gena-do-xingu): vie, traditions et culture"

[[links]]
link = "Petit témoignage de [Alessandra du peuple Munduruku](https://youtu.be/CnDx3veBZaU), vidéo YouTube"

[[links]]
link = "L'agenda ultralibérale de Bolsonaro: assassinats des protecteurs de la forêt et [piller les richesses de l'Amazonie](https://www.bastamag.net/Amazonie-Bresil-Bolsonaro-autochtones-indigenes-incendies-climat)"

[[links]]
link = "[Le combat contre les mines d'or en Guyane](https://reporterre.net/En-Guyane-le-combat-contre-les-mines-d-or-industrielles-continue), article de janvier 2021 par reporterre, le quotidien de l'écologie"
+++
Suite à mon article consacré à Ailton Krenak, j'ai découvert d'autres défenseurs des droits humains et de l'environnement d'Amazonie. Voici leur petit portrait. Ils sont du Brésil et Pérou. 

# Chef Raoni, du peuple Kayapó

Chef et guerrier Raoni Metuktire (né vers 1932) a fait le tour du monde pour sensibiliser sur la lutte des peuples indigènes et nous alerte depuis plusieurs décennies (depuis 1989!) sur la déforestation, refuse les projets de barrage hydroélectrique aux conséquences écologiques désastreuses. 

Il se bat pour la démarcation et la protection des territoires ancestraux où peuples autochtones et le vivant présent dans la forêt peuvent enfin vivre en paix. Il prône l'unification des peuples indigènes, même s'ils étaient parfois historiquement antagonistes, pour ensemble protéger la forêt.

Une de ses grandes victoires est l'unification de plusieurs réserves à l'existant Parc indigène de Xingu.

<iframe title="vimeo-player" src="https://player.vimeo.com/video/297702955" width="640" height="360" frameborder="0" allowfullscreen></iframe>

# Les frères Villas-Bôas

Le Parc Indigène de Xingu est la parfaite transition pour présenter les trois frères indigénistes Orlando, Claudio et Leonardo qui, dans les années 1950, établirent le premier contact pacifique avec le jeune Raoni et sa tribu.

En 1943, les jeunes frères prennent part à un programme étatique appelé "Marche vers l'ouest" et entamèrent une expédition dans l'Amazonie. Ils vont établir le premier contact avec plusieurs peuples isolés de la forêt. 

Leur ouverture d'esprit et humanisme créent un climat de confiance. En vivant avec les peuples indigènes, un échange réciproque de connaissances s'opère et une amitié se développe.

Conscients du caractère purement vénale et des conséquences néfastes de la mission de la "civilisation brésilienne" en Amazonie au nom du progrès, ils savent qu'ils sont "le venin et l'antidote".

Le venin car ils participent, même si indirectement, au processus de dépossession des terres indigènes par les blancs. Et puis, ils sont porteurs des maladies qui touchent gravement les peuples autochtones non immunisés.

L'antidote car ils apportent soins médicaux et mettent en place un plan pour la création d'un territoire pour les peuples indigènes. En 1961, les frères Villas-Bôas vont contribuer à la création du Parc Indigène du Xingu couvrant 26 000 km2, habité par 16 groupes ethniques différents. Cette audacieuse idée de "territoire" des frères Villas-Bôas est aujourd'hui une solution permettant aux peuples indigènes d'exister et l'une des meilleures solutions pour sauver la forêt si la démarcation se réalise comme le stipule la constitution brésilienne.

![Les frères Villas-Bôas](/media/uploads/villasboas.webp "Les frères Villas-Bôas")

# Joenia Wapixana

Joenia Wapixana (née le 20 avril 1974) est la première femme indigène membre du congrès brésilien (élue en 2018) et aussi la première avocate indigène (1997). Elle joue un rôle important dans la lutte pour la démarcation des terres indigènes, ancrée dans la constitution brésilienne de 1988 mais dont la progression est très lente. Elle est omniprésente dans l'arène parlementaire pour s'opposer à la "nécropolitique" du gouvernement.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Jk-rn7J6d7U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Nécropolitique

Dans nos sociétés modernes, est ce qu'un État détient le pouvoir de faire mourir, de laisser vivre ou d'exposer à la mort? Légifère t-il ou refuse t-il de légiférer en fonction de la réponse à un questionnement "quelle vie importe"?

[Achille Mbembé](https://www.franceculture.fr/personne-achille-mbembe.html) (philosophe, historien, politologue et enseignant universitaire né en 1957) nous éclaire sur ce "pouvoir de tuer exercé par le souverain" avec le concept de nécropolitique (voir lien 1 à la fin de l'article)

Au Brésil, on assiste bel et bien à la mise en œuvre d'une politique d'exclusion d'un ennemi fictionnel en fonction de son ethnicité, son genre ou son orientation sexuelle. Les pratiques de pouvoir sont celles de l'extrême droite: racisme, climato-scepticisme et autoritarisme, discours de haine envers peuples indigènes ou ONG environnementales. La condition "d'état d'exception" est également remplie puisque l'État par son laisse faire, rend ainsi acceptable le "droit de tuer" dans les forêts et favelas.

Sur le plan environnemental, ce laisser faire se traduit par l'extrême lenteur des travaux de démarcations (le gouvernement tentant même de lever les obstacles constitutionnels afin de suspendre voire annuler le démarquage des territoires autochtones) et la non sanction des incendies criminels. 

L'agenda ultralibérale est clair: piller les richesses de l'Amazonie. 

# Chico Mendes

![Chico Mendes (1944 - 1988)](/media/uploads/chico-1.jpg "Chico Mendes")

Dans la lutte pour le défense de l'Amazonie, les peuples indigènes vont trouver un allié: les "*seringueiros*". Ce sont des colons portugais autrefois ennemis des peuples indigènes, qui ont décidé de vivre dans la forêt pour exercer l'activité lucrative d'extraction de caoutchouc et la collecte de noix du Brésil. Au fil du temps, les seringueiros ont appris à respecter la forêt en l'exploitant de façon durable.

Le seringueiro Chico Mendes est, en 1985, l'instigateur d'une union des peuples de la forêt en réunissant extractivistes, ribeirinhos (peuples traditionnels vivant le long des fleuves et rivières), quilombolas (communautés majoritairement d'origine africaine mais aussi indigène, dont les ancêtres esclaves ont réussi à fuir les plantations) et les indigènes.

Chico est une figure centrale dans le combat social et environnemental qui se joue en Amazonie contre l'État et les riches propriétaires terriens. Avec ses compagnons du syndicat, il prône l'usage de méthodes pacifistes face à des propriétaires terriens et gardes armés.

Il obtient une victoire juridique en 1988 grâce à un argument économique: la création de réserve "extractiviste" dans lequel les habitants traditionnels ont le droit d'exploiter la forêt de façon durable et peuvent préserver leurs coutumes et traditions. 

Chico sera assassiné la même année par les hommes de main d'un grand propriétaire terrien. Il est encore aujourd'hui une icône de la lutte écologique. Dans le film "The Burning Season" (1994) l'acteur Raul Julia interprète le rôle de Chico. Une chanson en son hommage par le groupe mexicain Mana ["Cuando los ángeles lloran"](https://youtu.be/55ClEwZ6nFI).

# Sônia Guajajara

Sônia Guajajara (née le 6 mars 1974) est une militante écologiste et défenseuse des droits de l'homme. Elle reçut le prix de l'Ordre du Mérite Culturel en 2015. Elle est l'actuelle coordinatrice et une des leaders de l'APIB (Articulation de Peuples Indigènes du Brésil) qui rassemble plus de 300 ethnies du pays. Elle milite en faveur d'un écosocialisme et sa voix prend de l'ampleur dans l'espace politique au Brésil. 

Au niveau international, l'APIB dénonce par exemple les banques BNP et Société Générale qui financent (à plusieurs milliards d'euros) et donc coopèrent avec des grandes entreprises exportatrices de la filière bovine et du soja, sociétés complices de déforestation et tout à fait au courant des conflits dus aux pressions foncières (voir lien en bas de page).

Elle apparaît sur la photo illustrant cet article (source: Midia NINJA)

# Marçal de Souza "Tupa-i"

Voici une partie d'un discours du leader indigène Marçal de Souza ou "Tupa-i" adressé au Pape Jean Paul ll en Juin 1980:

> *Nossas terras são invadidas, nossas terras são tomadas, os nossos territórios são invadidos… Dizem que o Brasil foi descoberto. O Brasil não foi descoberto não, o Brasil foi invadido e tomado dos indígenas do Brasil. Essa é a verdadeira história*\
> *Nos terres sont envahies, sont prises, nos territoires sont envahis...Ils disent que le Brésil fut découvert. Le Brésil ne fut pas découvert, le Brésil fut envahi et prise aux indigènes du Brésil. C'est cela la vraie histoire*

![Tupa-i (1920 - assassiné en 1983)](/media/uploads/1564567837455.jpg)

# Davi Kopenawa, du peuple Yanomami

Le leader écologiste et chaman yanomami Davi Kopenawa (né vers 1956), [lauréat du Prix Nobel Alternatif 2019](https://www.survivalinternational.fr/actu/12292), lance un appel à l'internationale: les autochtones et la forêt sont en danger. Quand l'Amazonie, un des poumons de la Terre est en danger, *nous* l'humanité le sommes aussi.

# Marcos Veron du peuple Guarani, de la réserve du Takuara

> *Là est toute ma vie, là se trouve mon âme. Si vous me privez de cette terre, vous me prenez ma vie*

Ce charismatique chef guarani fut assassiné le 13 janvier 2003, à l'âge de 75 ans, lors d'une tentative de retour (retomada da terra) sur sa terre ancestrale violée par des intrusions et implantations illégales de grands propriétaires de ranchs, dans l’État du Mato Grosso do Sul à la frontière avec le Paraguay (voir lien en bas de page, article du Guardian).

# Máxima Acuña

La défenseuse de l'environnement et agricultrice du Pérou, Máxima Acuña, reçut en 2016 une distinction équivalente à un Prix Nobel de l'environnement ([Prix Goldman pour l'environnement](https://www.goldmanprize.org/recipient/maxima-acuna/)) pour saluer son courage face à une géante multinationale américaine.

![Maxima Acuna](/media/uploads/229752_máxima-acuña-cajamarca-perú-1349x900.jpg "Maxima Acuna")

En 2011, elle et sa famille sont violemment expulsées de leur terre par une milice pour le compte d'une entreprise de mines d'or qui s'approprie également le lac avoisinant, et ce, avec la complicité de la police locale (voir lien en bas de page pour plus d'infos).

A présent, une longue et éprouvante bataille judiciaire a lieu devant les tribunaux américains. D'abord jugée coupable, le dernier procès lui fut favorable, obligeant l'arrêt du projet minier. Le Pérou, aveuglé par une vision ultralibérale, cède aux multinationales sa forêt amazonienne et ignore les droits des peuples indigènes.

En Bolivie, les nations indigènes sont constitutionnellement reconnues, l'État bolivien est plurinational, le fameux drapeau wiphala représente les peuples indigènes d'Amérique latine. Au Chili, un référendum de 2020 permet la réécriture de la constitution. Le peuple indigène Mapuche qui se bat de longue date, espère enfin une reconnaissance constitutionnelle de leurs droits et territoires.

En Guyane, la France n'a pas signé la convention 169 de l'Organisation Internationale du Travail de 1989, une convention qui pourtant protège et accorde des droits aux peuples indigènes (voir liens en bas de page pour plus d'infos). Les innombrables projets de mines d'or toxiques (pollution engendrée par le cyanure, déforestation, stockage de déchets miniers via des barrages) en Guyane démontre le peu d'intérêt de la France pour préserver la forêt ou respecter les peuples indigènes.

# Une action locale pour un impact global

Le combat des peuples indigènes pour la reconnaissance, la dignité et pour l'accès aux terres ancestrales au Brésil aussi bien que dans les autres pays d'Amérique du sud semble avoir plus de visibilité. Alors que dans le même temps les pressions s'intensifient (le cours de l'or est au plus haut galvanisant les orpailleurs illégaux) et les tensions s'aggravent.

*Pour moi, ce sont eux, ces gardiens de la forêt, les vrais écologistes. En agissant avec simplicité localement en préservant la forêt, ils impactent positivement globalement.*

Prenons conscience nous aussi, habitants du "Nord", que chacune de nos actions locales ont un impact global. Voici des slogans autant pour lutter contre le racisme que pour d'autres injustices:

> Silence is Violence \
> Silence Is not an option \
> Silence is complicity 

L'actualité nous démontre que le silence c'est faire le jeu d'un système agresseur et n'aide aucunement à apporter le changement, un changement pourtant urgent.

***Par nos actions, notre écoute et nos voix, nous pourrions être les alliés des peuples indigènes.***

Adelino Duarte, le 10 mars 2021