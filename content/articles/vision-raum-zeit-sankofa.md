+++
title = "Vision, Raum, Zeit und Sankofa"
slug = "vision-raum-zeit-sankofa"
draft = true
language = "de"
themes = ["Society"]
date_published = "2020-07-28"
image = "/media/uploads/img_7793.jpg"
image_caption = "Sonnenuntergang in Berlin"

[[links]]
link = "Das Buch [Afrotopia](https://www.deutschlandfunkkultur.de/felwine-sarr-afrotopia-schluss-mit-westlichen-kriterien.1270.de.html?dram:article_id=439817) von Felwin Saar, 2018: Schluss mit westlichen Kriterien!"

[[links]]
link = "[Die Rolle der Frauen in der covid Krise](https://www.klassegegenklasse.org/francoise-verges-ohne-die-frauen-die-die-welt-putzen-kann-nichts-funktionieren/), Francoise Vergés"

[[links]]
link = "ZDF \"Planet E\" [Reportage](https://www.zdf.de/nachrichten/panorama/coronavirus-zoonose-artenschutz-100.html): \"Artenschutz als Helfer im Kampf gegen Viren\"."

[[links]]
link = "[Fleisch und Soja: Warum Massentierhaltung den Regenwald bedroht](https://www.abenteuer-regenwald.de/bedrohungen/fleisch-soja), Abenteuer Regenwald Erlkärung"

[[links]]
link = "[Sankofa](https://de.wikipedia.org/wiki/Sankofa) Bedeutung"

[[links]]
link = "Malcom Ferdinand, [Why we need a decolonial ecology](https://www.greeneuropeanjournal.eu/why-we-need-a-decolonial-ecology/)"

[[links]]
link = "Chlordecon sanitäre und umwelt Krise, Artikel \"[Das Pestizid, das aus dem Wasserhahn tropft](https://www.zeit.de/wissen/gesundheit/2018-06/pestizid-chlordecon-gift-glyphosat-antillen)\" im Zeit.de"
+++
## Vision

*"Eine entfernte, unscharfe, unsichtbare Vision. Denn ich kann nicht sehen.* 

*Oder hingegen eine klare und sichtbare Vision. Aber ich will nicht sehen."*

Ich möchte diesen Artikel im Kontext situieren: Ich habe ihn zunächst Mitte April 2020 auf Französisch veröffentlicht. Er bezieht sich hier auf meine Gedanken während der Covid Pandemie. Das war eine ziemlich besorgniserregende Zeit, die geeignet für Reflexion war. Das war mindestens bei mir der Fall da ich viel überlegen habe. Erst jetzt im Juli nehme ich mich vor, die Deutsche Übersetzung zu bearbeiten.

Es kann nun rückblickend festgestellt werden, dass innerhalb von nur drei Monaten viel passiert ist! 

Könnte sich aus den schlimmen Ereignissen ein Besseres ergeben? Auf Französisch spricht man über "Le Monde d’après" nämlich die Welt danach.

Dieser Artikel sollte ursprünglich auf leichte Weise das Thema der täglichen Verhalten ansprechen. Und zwar unsere Verhalten, die eine Auswirkung auf die Umweltbelastung minimieren und die, die eine positive Auswirkung auf den sozialen Fortschritt maximieren. Es heißt, wie kann man Bemühungen unternehmen um ökologischer und ethischer zu werden. 

Ich muss nun zugeben, dass diese vor allem "westliche“ Denkweise unserer Realität einer aktuellen vielfältiger und komplexer Welt überhaupt nicht entspricht.

Menschen können wohl verantwortungslos agieren, indem sie die Erde zerstören: Wir buchstäblich übertragen unserer eigenen Erde das Fieber und wir plündern ihre "grüne Lungen". Vielleicht könnten wir uns anders verhalten, sodass wir letzendlich die Erde atmen lassen? Daraus könnte sich ein Wohlergehen allen Leben auf der Erde ergeben und damit ein Wohlergehen unserer künftigen Generationen.

Dann kam dieses unsichtbare Virus. Im Zuge meines Schreibens war es mir klar geworden, dass dieser naive und phantasievolle Begriff "grüne Verhalten" keinen Sinn machte. 

Ich bevorzuge aber den schlagkräftigen und eindeutigen Ansatz eines verantwortungsvolles und gerechtes Verhalten. Folglich kommt natürlich zur Sprache das Thema der Schuld bezüglich der ökologischen Katastrophe. Die Frage stellt sich nach wer eigentlich die Opfer sind.

Diese Pandemie klingt wie einer von den bereits unzähligen Alarmzeichen. Nehmen wir es dieses Mal wahr? Machen wir wie immer die Augen zu? Die jüngst sanitäre Krise schien als der ideale Zeitpunkt für Reflexion zu sein. Eine Zeit für eine kritische Hinterfragung unseren gesellschaftlichen beherrschenden Werten.

![Der Rufer, Berlin](/media/uploads/rufernew.jpeg)

Verwöhnt in unserer Globalisierung, schauen wir aber die Welt nicht richtig an. 

Als wir unseren Kaffee und köstliche Schokolade genießen, verweigert sich unsere "westliche" europäische Sicht, die Verletzungen der Menschen, die tatsächlich unser klein tägliches Vergnügen produzieren, zu beachten. 

Mir ist von nun an bewusst, dass solcher Begriff "grünes Verhalten" bei der autochthonen Gemeinschaften überhaupt nicht bedeutet. Und dieses Verständnis gilt ebenfalls bei der ausgebeuteten Menschen aus Afrika und Asien und sogar auch, noch uns näher, bei der armen Familien von der vermeintlichen reichen westlichen Länder. 

Während der Covid Pandemie waren die signifikante Aktivitäten in allen Länder von Arbeitern mit Niedriglohnjobs gestützt worden. Darüber hinaus waren die "System Relevant" Arbeiten bei 80% von Frauen besetzt: Frauen sind tatsächlich mehr ausgesetzt und mehr gefährdet vor dem Virus. 

Außerdem wird die Gewalt gegen die ökologische Aktivisten in der südlichen Länder eines Tages ernst genommen? Angehörige der autochthonen Gemeinschaften in Amazonas, die Hüter des Waldes und Schützer der Umwelt sind schweigend ermordert.

Wer die Natur wirklich respektiert, der kümmert sich de facto um sich selbst. Das Ökologisches sollte zusätzlich der Frage des Soziales begegnen. Es geht um die Toleranz gegenüber dem Anderen zu fördern. Eine Welt wo überwiegt eine umfassende Anerkennung des Unterschieds und der Vielfalt. 

Eine Welt wo man auf anderen Mensch unabhängig vom Geschlecht, sexueller Orientierung, Kultur, Hautfarbe und gesellschaftlicher Stellung Rücksicht nehmen. Dem Anderen und der Natur einfach Raum lassen.

![Fantastische Erinnerung von Äthiopien, ein einzigartiges Land mit umfangreicher Kultur](/media/uploads/ethionew.jpeg)

Unsere Geschichte enthüllt diese folgende Beobachtung: Es gibt eine egoistische Haltung, die leider zu einem Zerfall führt. Und zwar der Mensch spaltet sich von der Natur ab und positioniert sich über sie. Dieser gefährliche Bruch mit der Natur, begründet durch eine vermeintliche Hierarchie der Spezies (die beherrschende Theorien in Europa im 18. Jahrhundert) womit der Mensch an der Spitze von der Pyramide herrscht, erlaubte dem Mensch halt seine Umwelt unbegrenzt auszubeuten und deshalb sie zu zerstören. 

Dieses Denksystem diente sogar als Begründung für eine widerliche Klassifizierung der menschlichen Wesen! Anschließend wurden einige von denen als Untermensch betrachtet und behandelt. Eine Entmenschlichung: Sie seien nur Körper ohne Seele und Würde.

## Raum

*Der Abstand, eine Geste des Schutzes gegenüber unseren vulnerabel Menschen und unserer fragilen Umwelt.*

Der coronavirus verpflichtet unseren menschlichen Spezies für eine unnatürliche Sozialdistanzierung. Es handelt sich um ein kollektives Verhalten damit die Ansteckung sich verlangsamt und dadurch Leben zu schützen. Denn unsere unzureichende ausgestattete Krankenhäuser könnten überfordert werden. So ist derzeit unsere lediglich Geste für Solidarität und Überleben.

Eine temporale Parenthese rund um Thema "Virus": Die Siedler und Kolonisten, die im Jahr 1492 in Amerika mit Christoph Columbus kamen (und nicht "entdeckten"!), brachten ihre Viren aus Europa mit, gegen die die indigene Gemeinschaften nicht immunisiert waren. Solche massive Pandemie im Zuge der unmenschlichen Kolonisierung vernichte einen Großteil der Indigenen Bevölkerung.

Wieder der Blick auf die Gegenwart: Der Virus macht deutlich, dass der Mensch einen gewissen Abstand gegenüber der Natur einhalten muss. Experten warnten uns bereits erfolglos: Der Mensch überwindet die Artenschranke. Der Mensch ist somit von neuen Viren ausgesetzt, die aus Tieren stammen. Diese arme Tiere, meistens vom Aussterben bedroht, werden wiederum Opfer der Habgier von Menschen, die illegaler Tierhandel ausüben. 

Der Wald muss wild bleiben und vor menschlichem Eindringen bewahrt. Einerseits, ist die autochthone Bevölkerung die wahre Hüter des Waldes. Dank ihren uralte Tradition besitzen sie das Verständnis der Natur und wissen genau, dass Grenzen immer irgendwo bestehen. Andererseits, verlangen die rasche unbeschränkte Urbanisierung und industrielle Landwirtschaft hingegen immer mehr Oberfläche.

Diese intensive industrielle Landwirtschaft dient überwiegend die Rinderzucht und die Futtermittelindustrie. Die verheerende Folgen der Entwaldung auf die Klimaerwärmung wurden schon seit langem in Bezug auf zwei Probleme  bekannt: Der Verlust und das Entweichen der CO2 aufgrund der abnehmenden Zahl von Bäume und zweitens das Artensterben. 

Wir erfahren nun eine weitere schlimme Auswirkung auf sanitärer Ebene. Meinerseits, höre ich den Fleischkonsum auf. 

![Eine Stimme, die bestimmt den Raum durchquert. Amy Whinehouse auf eine Strasse in Camden, London](/media/uploads/img_4498.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/nMO5Ko_77Hk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Ich

Es wird häufig ausgedrückt, dass der Verbraucher eine wichtige Rolle bei der Änderung unserer Gesellschaft spielt. 

Soll der Weg zu einer umwelt- und menschenfreundlichen Gesellschaft obligatorisch durch das lediglich Kriterium des Verbrauchs zustande kommen? Wenn die Ressourcen nicht unendlich sind und deshalb erschöpfbar? 

Darüber hinaus denken wir gezielt an die Menschen, die nicht genügend finanzielle Mittel haben damit sie eine kraftvolle Stimme durch ihre Verbrauch erwerben können (die wichtige Fragestellung der soziale Ungerechtigkeit)?

***Weniger passiv Verbraucher sein, sondern aktiver Bürger***

Ein verantwortungsvolles Verbrauchen einzuleiten ist konkret ein guter Beginn. Jeder wählt sein Rhythmus. Es heißt weniger verbrauchen wenn notwendig, anders verbrauchen, besser verbrauchen in Sachen Energie, Transport und Ernährung. 

Die Aufmerksamkeit richtet sich vor allem auf ökologisch erzeugte Lebensmittel, lokale und ethische Geschäfte, Energieeffizienz, Energieeinsparung, erneuerbare Energie, die Verringerung des CO2 Fußabdrucks bezüglich meiner Mobilität und der Möglichkeit häufiger mit dem Zug zu fahren…

![Mein Lieblingsverkehrsmittel in Berlin, mein Fahrrad hergestellt im Ozoncyclery Workshop](/media/uploads/dscf2839.jpg)

Die obene erwähnte Alternative beinhalten einen maßgeblich menschlichen und ethischen Mehrwert. Derjenige, der einen Preis hat. Zum Beispiel verbietet die bäuerliche Landwirtschaft die Nutzung umweltschädliche Pestizide und könnte sogar tausende Arbeitsplätze schaffen. 

Im Gegensatz dazu bevorzuge der umweltschädliche robotisierte für Exportausrichtung Agrarbetrieb die Monokultur. 

Man soll Lösungen finden sodass alle von uns einen Zugang zu diesen ethischen und gesunden Produkten erhalten können. Es heißt nicht nur eine effiziente lokale Versorgung sondern auch eine höhere Kaufkraft. Hier kommt das Thema der Umverteilung zur Diskussion.

![Der Garten meines Vaters. "Man soll seinen Garten anbauen". Ebenfalls eine schöne Philosophie aus Cabo Verde.](/media/uploads/papanew.jpeg)

Der Bürger könnte zwar einen Einfluss auf die Ebene der Verbrauchsmuster haben. Aber ich bin der Meinung, dass dieser Einfluss zurzeit ziemlich begrenzt ist. Denn andere wirtschaftliche Beteiligte leisten starke Widerstand gegen irgendeine Übergang des Verbrauchsverhalten. Ihnen zufolge seien die Sache nicht zu ändern und wir hätten keine andere Wahl außer "Business as usual" zu betreiben (TINA, There Is No Alternative). 

Unsere Produktionsorientierte und Verbrauch- Gesellschaften benötigen zwar immer (aber weniger aufgrund der Robotisierung) Arbeiter aber (immer mehr ständiger) Verbraucher. 

Zudem interessiert sich unsere Gesellschaft scheinbar nicht für verantwortungsvolle Bürger. Und noch dazu wird der finanzielle Neoliberalismus vollkommen verrückt wie ein virtueller Kasino gedopt von Algorithmen, die kurzsichtig Gewinn (durch Nanosekunde Handel) sucht.

Alles völlig getrennt von der Realität einer produktiven Wirtschaft unseres bescheidenen Lebens, darauf wir uns natürlich langfristig und nachhaltig besinnen.

***Und wir?***

Wenn es um die Frage nach einem Aufbau einer neuen Gesellschaft geht, ist meiner Meinung nach eine gute Steuerung gleichermaßen durchaus wichtig. Eine Orientierung zu einer effizienten Umwelt- und Sozialpolitik benötigt beispielsweise massive ökologische Investierung, Unterstützung der umweltfreundlichen Unternehmen und Versteuerung der umweltschädlichen Aktivitäten. 

Ich stelle mir vor eine neue Gesellschaft beruht auf mehr lokale ökologische Betriebe, Aufgeschlossenheit, Gastfreundschaft und Kooperation zwischen Ländern. 

Ein stark politischer Wille ist deswegen unverzichtbar denn der Staat muss in der Lage sein, sich gegen den ausgeübten Drücken von Konzernen und der extreme Finanzialisierung der Wirtschaft zu wehren. Der alleine Verbraucher scheint leider machtlos zu sein.

Musikalische Pause: "Hora Ja Tchega" (Die Zeit ist angekommen).

<iframe width="560" height="315" src="https://www.youtube.com/embed/2-cy9kwarjU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

> *Africa ja corda* 
>
> *Nos força ja duplica* 
>
> *Negro ja grita vitoria* 
>
> *Medo ja trancas na corpe* 
>
> *Hora ja tchega* 
>
> *Africa ja corda* 
>
> *Medo ja ca tem* 
>
> *Pa no bem luta té fim* 
>
> *Nos vitoria ta na mon* 
>
> *Graças a nos irmon Cabral* 
>
> *Qui derrota colonialismo* 
>
> *Pa liberta se pove irmon*

Heutzutage wird die neoliberale Ideologie so gelobt, dass die Regierungen  mehr an Kommodifizierung und Handel glauben scheinen als an eine authentisch individuelle Freiheit und an das Wohlergehen der Bevölkerung.

Freizügigkeit des Warenverkehrs und freier Kapitalverkehr: Die politische Anführer applaudieren, verabschieden Gesetze und unterzeichnen Freihandelsabkommen.

Aber wenn es um die Rettung von Flüchtlingen im Meer und Leben zu retten geht, kehren die selben Politiker den Rücken! Warum schließlich die Solidarität gegenüber den Flüchtlingen im Erde und im Meer zu kriminalisieren?

Erklären Sie uns "Festung Europa". Eine Schande, daran wir uns Alle leider teilnehmen…

***Sankofa***

Es ist höchst Zeit für eine Entkolonisierung.

Ich bin der Auffassung, dass die Welt danach sich erstens durch eine dringende geistige Entkolonisierung verwirklichen wird. Der Mensch bedürft ein neues Wahrnehmen und eine neue Denkweise. 

Diese Welt erfordert auch eine physische Entkolonisierung durch die Art und Weise wie der Mensch die Erde bewohnt. Malcom Ferdinand spricht das Thema "Die Erde bewohnen" in seinem Buch "Une écologie décoloniale" an, in dem er den zweifachen Bruch der Modernität vorstellt. 

Seine Forschung enthüllt nicht nur einen kolonialen Bruch sondern auch einen einer ökologische Art. Er weist die koloniale Aspekte der "westliche" Ökologie aus: [](https://www.greeneuropeanjournal.eu/why-we-need-a-decolonial-ecology/)

[Der Kampf um eine dekoloniale Umweltgerechtigkeit. Mit Malcom Ferdinand](https://www.goethe.de/prj/lat/de/dtl/med.html) (auf Englisch)

*Meine eigene Vorstellung und meinen (Welt)Raum entkolonisieren.*

Temporale Parenthese: Die koloniale Unternehmung und ihre logische Folge (ein imperialistisches und kapitalistisches System) könnten sich ohne die Vernichtung der ersten Bevölkerung von Amerika, ohne die in großem Umfang Sklaverei und ohne die massive Ausbeutung der Natur, nicht verwirklichen. Vielen scheinen es noch heute damit zu leben. Aber wir vergessen nicht.

Die Geschichte zu entkolonisieren und Raum für neue Erzählungen: *Der "Zeit" Faktor.* Der Sankofa Geist: "Die Vergangenheit betrachten um die Zukunft aufzubauen"

![New York, die damals New Amsterdam Stadt, wo Sklaven von Holländer versteigert wurden. Sie wurden dann Hausdiener.](/media/uploads/nyafrika.jpeg)

Eine Kolonisierung von mehrere Jahrhunderte lässt in unserer aktuellen Gesellschaft sichtbare Spüren hinter. Die Kolonisierung hat unsere Gesellschaft schwer geprägt. 

Aus der bestehenden kolonialen Denkweise ergeben sich Wörter aber auch Ungesagten, zum Schweigen bringen und schlimme Handlungen. Auch Demütigungen, Unsichtbarkeit der Minderheiten (z.B die sanitäre chlordecon Krise wobei das Trinkwasser der französischen Antillen-Inseln Guadeloupe und Martinique nachhaltig pestizidbelastet ist) und Ungerechtigkeiten.

Die koloniale Denkweise besteht darin zu denken, dass es Bürger zweiter Klasse gebe, die in Peripheriezone leben. In dieser physischen Randzone wurden Experimente auf die Bevölkerung und auf die natürliche Elemente durchgeführt. In Afrika für Behandlungen und Medikamententests, im Amazonas Wald, in Vororte angesichts Polizeigewalt, auf den Antillen Inseln, in Vietnam, im Pazifik Ozean in Bezug auf Atomtest. 

Diese schlechte und beleidigende koloniale Denkweise hat sich seit der Covid Pandemie leider deutlich verschärft.

!["We came to America" von Faith Rinngold, Austellung "The Color Line", Paris](/media/uploads/statueliberty.jpeg)

Toleranz und Raum für Alle

Ich vertrete die Ansicht, dass der Mensch grundsätzlich ein Teil der Natur ist, inmitten eines komplexen Ökosystem. Die Betrachtung unserer Natur von der Spitze unserer Überheblichkeit führt uns zu einer Katastrophe. 

Die unbegrenzte Ausbeutung der Umwelt (eine Kolonisierung der Erde) und die Kolonisierung der Menschen erinnern uns daran, dass sie beide den Unsinn und das Symbol einer Ungerechtigkeit darstellen, die nach wie vor in unserer aktuellen Gesellschaft andauern.

Die Fragestellung nach Umweltschutz, Sozialem Fortschritt und Kampf gegen Diskriminierungen und Rassismus sind unbestreitbar eng miteinander verbunden: Intersektionalität. Sie nähern sich zu einem gleichen Ideal für Gerechtigkeit an und tragen zu einem Wohlergehen in allen Kontinente bei. 

![Das Buch Afrotopia von Felwine Sarr](/media/uploads/felwinecafe.jpeg)

***Seine eigene Vorstellungskraft gestalten und damit (sich) inspirieren.*** 

*Für meinen Onkel Geraldo Dongo (1945-3 Mai 2020).*

Adelino Duarte, am 28. Juli 2020