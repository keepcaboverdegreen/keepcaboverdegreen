import collections from './collections.js'

export default {
    config: {
	// Skips config.yml.
	// By not skipping, the configs will be merged, with the js-version taking priority.
	load_config_file: false,
	display_url: window.location.origin,

	backend: {
	    name: 'gitlab',
	    branch: 'main',
	    repo: 'keepcaboverdegreen/keepcaboverdegreen',
	    auth_type: 'pkce',
	    app_id: '58b90e74d87d27816a2ee26df9d5595641b17139b821f314d1371f4566974a39'
	},

	media_folder: 'static/media/uploads',
	public_folder: '/media/uploads',

	collections
    }
}
