import title from './field-title.js'
import slug from './field-slug.js'
import description from './field-description.js'
import index from './field-index.js'

const themes	= {
	format: 'toml-frontmatter',
	name: 'themes',
	label: 'Themes',
	label_singular: 'Theme',
	folder: 'content/themes',
	path: '{{slug}}/_index',
	create: true,
	slug: '{{title}}',
	editor: {
		preview: false
	},
	fields: [
		title,
		slug,
		index,
		description
	]
}

export default themes
