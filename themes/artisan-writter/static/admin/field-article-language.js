export default {
    label: 'Language',
    name: 'language',
    widget: 'select',
    required: true,
    default: 'fr',
    hint: 'What is the language of the article?',
    options: [
	{
	    label: 'Creole',
	    value: 'cr'
	},
	{
	    label: 'English',
	    value: 'en'
	},
	{
	    label: 'Portuguese',
	    value: 'pt'
	},
	{
	    label: 'French',
	    value: 'fr'
	},
	{
	    label: 'German',
	    value: 'de'
	}
    ]
}
