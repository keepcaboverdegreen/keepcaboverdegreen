/* just like field body, but does not saveto thebody key (used by
default by SSG as .Content) */
export default {
    label: 'Description',
    name: 'description',
    widget: 'markdown',
    required: false,
    hint: 'Write a description about this item. Free text and medias.'
}
