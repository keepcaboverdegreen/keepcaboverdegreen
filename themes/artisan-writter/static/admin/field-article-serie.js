export default {
    label: 'Serie',
    name: 'serie',
    widget: 'relation',
    collection: 'series',
    default: '',
    searchFields: ['title', 'slug'],
    valueField: 'slug',
    displayFields: ['title'],
    hint: 'To which serie belong this item?',
    required: false,
}
