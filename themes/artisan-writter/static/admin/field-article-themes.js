export default {
    label: 'Themes',
    name: 'themes',
    widget: 'relation',
    collection: 'themes',
    default: '',
    searchFields: ['title', 'slug'],
    valueField: 'slug',
    displayFields: ['title'],
    hint: 'To which themes belong this item?',
    required: false,
    multiple: true
}
