export default {
	label: 'Index',
	name: 'index',
	widget: 'number',
	value_type: 'int',
	min: 1,
	max: 99,
	required: false,
	hint: 'The lower this number, the earlier in the list the item shows'
}
