export default {
	label: 'Draft',
	name: 'draft',
	widget: "boolean",
	required: false,
	default: false,
	hint: 'If draft is set to true, this page will not be published on the live site.',
}
