import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import featuredImage from './field-featured-image.js'

const pages = {
    name: 'pages',
    label: 'Pages',
    label_singular: 'Page',
    format: 'toml-frontmatter',
    create: true,
    folder: 'content/',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	featuredImage,
	body
    ]
}

export default pages
