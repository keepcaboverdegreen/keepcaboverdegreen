import title from './field-title.js'
import slug from './field-slug.js'
import description from './field-description.js'
import featuredImage from './field-featured-image.js'

const series  = {
    format: 'toml-frontmatter',
    name: 'series',
    label: 'Series',
    label_singular: 'Serie',
    folder: 'content/series',
    path: '{{slug}}/_index',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	featuredImage,
	description
    ]
}

export default series
